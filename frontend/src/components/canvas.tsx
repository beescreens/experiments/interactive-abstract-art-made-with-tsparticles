import Particles from "react-particles";
import { Container, Engine, IOptions, RecursivePartial, Particle } from "tsparticles-engine";
import { loadFull } from "tsparticles";
import { useCallback, useEffect, useRef, useState } from 'react';
import { loadSeaAnemonePreset } from "tsparticles-preset-sea-anemone";
import React from "react";
import { useWebSocket } from "@/components/websocket-provider";


export const Canvas = () => {
    const { socket, emitPoint } = useWebSocket();

    socket.on('connection', () => {
        console.log('coucou')
    })

    const options: RecursivePartial<IOptions> = {
        fpsLimit: 60,
        interactivity: {
            detectsOn: "window",
            events: {
                onClick: {
                    enable: true,
                    mode: "emitter"
                }
            },
        },
        preset: "seaAnemone",
    }

    const optionsConfetti: RecursivePartial<IOptions> = {
        fpsLimit: 60,
        interactivity: {
          events: {
            onClick: {
              enable: true,
              mode: "emitter"
            }
          },
          modes: {
            emitters: {
              direction: "none",
              spawnColor: {
                value: "#ff0000",
                animation: {
                  h: {
                    enable: true,
                    offset: {
                      min: -1.4,
                      max: 1.4
                    },
                    speed: 0.1,
                    sync: false
                  },
                  l: {
                    enable: true,
                    offset: {
                      min: 20,
                      max: 80
                    },
                    speed: 0,
                    sync: false
                  }
                }
              },
              life: {
                count: 1,
                duration: 0.1,
                delay: 0.6
              },
              rate: {
                delay: 0.1,
                quantity: 100
              },
              size: {
                width: 0,
                height: 0
              }
            }
          }
        },
        particles: {
          number: {
            value: 0
          },
          color: {
            value: "#f00"
          },
          shape: {
            type: ["circle", "square", "polygon"],
            options: {
              polygon: {
                sides: 6
              }
            }
          },
          opacity: {
            value: { min: 0, max: 1 },
            animation: {
              enable: true,
              speed: 1,
              startValue: "max",
              destroy: "min"
            }
          },
          size: {
            value: { min: 3, max: 7 }
          },
          life: {
            duration: {
              sync: true,
              value: 7
            },
            count: 1
          },
          move: {
            enable: true,
            gravity: {
              enable: true
            },
            drift: {
              min: -2,
              max: 2
            },
            speed: { min: 10, max: 30 },
            decay: 0.1,
            direction: "none",
            random: false,
            straight: false,
            outModes: {
              default: "destroy",
              top: "none"
            }
          },
          rotate: {
            value: {
              min: 0,
              max: 360
            },
            direction: "random",
            move: true,
            animation: {
              enable: true,
              speed: 60
            }
          },
          tilt: {
            direction: "random",
            enable: true,
            move: true,
            value: {
              min: 0,
              max: 360
            },
            animation: {
              enable: true,
              speed: 60
            }
          },
          roll: {
            darken: {
              enable: true,
              value: 25
            },
            enable: true,
            speed: {
              min: 15,
              max: 25
            }
          },
          wobble: {
            distance: 30,
            enable: true,
            move: true,
            speed: {
              min: -15,
              max: 15
            }
          }
        },
        detectRetina: true
      }
    
    const particlesInit = useCallback(async (engine: Engine) => {
        console.log(engine);

        // you can initialize the tsParticles instance (engine) here, adding custom shapes or presets
        // this loads the tsparticles package bundle, it's the easiest method for getting everything ready
        // starting from v2 you can add only the features you need reducing the bundle size
        await loadFull(engine);
        await loadSeaAnemonePreset(engine);
    }, []);

    const particlesLoaded = useCallback(async (container: Container | undefined) => {
        console.log(container);

        container?.addClickHandler((e: Event) => {
            const event = e as MouseEvent | PointerEvent;

            console.log(`Emit particule at [x: ${event.clientX}, y: ${event.clientY}]`)

            emitPoint({
                x: event.clientX,
                y: event.clientY,
            });
        });

        const canvas = container?.interactivity.element
        console.log('canvas', canvas);

        canvas?.dispatchEvent(new PointerEvent("click", {
            view: canvas as Window,
            bubbles: true,
            cancelable: true,
            clientX: 40,
            clientY: 50,
        }));   

        setContainer(container)
    }, []);

    const [container, setContainer] = useState<Container>();
    useEffect(() => {
        if (!container) return
        const canvas = container.interactivity.element
        console.log('canvas', canvas);

        canvas?.dispatchEvent(new PointerEvent("click", {
            view: canvas as Window,
            bubbles: true,
            cancelable: true,
            clientX: 50,
            clientY: 50,
        }));        

    }, [container]);

    return (
        <Particles options={optionsConfetti} init={particlesInit} loaded={particlesLoaded} />
    );
};

