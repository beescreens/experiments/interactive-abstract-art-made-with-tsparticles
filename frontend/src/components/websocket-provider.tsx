import { createContext, useContext, useEffect, useMemo } from "react";
import { Socket, io } from "socket.io-client";
import { Point } from "@/types/point";

type State = {
    emitPoint: Function,
    socket: Socket,
};

const WebSocketContext = createContext<State | undefined>(undefined);

export type WebSocketProviderProps = {
    backendUrl: string;
    children: React.ReactNode;
};

function WebSocketProvider({
    backendUrl,
    children,
}: WebSocketProviderProps): JSX.Element {
    const socket: Socket = useMemo(
        () => io(backendUrl, { autoConnect: false }),
        [backendUrl]
    );

    useEffect(() => {
        if (!socket.connected) {
            socket.connect();
        }
        return () => {
            socket.close();
        };
    }, [socket]);

    const emitPoint = (point: Point) => {
        socket.emit("POINT", point);
    };

    return (
        <WebSocketContext.Provider value={{ socket, emitPoint }}>
            {children}
        </WebSocketContext.Provider>
    );
}

function useWebSocket(): State {
    const context = useContext(WebSocketContext);
    if (context === undefined) {
        throw new Error("useWebSocket must be used within a WebSocketProvider");
    }
    return context;
}

export { WebSocketProvider, useWebSocket };