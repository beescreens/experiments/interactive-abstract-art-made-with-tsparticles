
import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { WebSocketProvider } from "@/components/websocket-provider";
import { Canvas } from "@/components/canvas";

export type HomeProps = {
  backendUrl: string;
};

export const getServerSideProps: GetServerSideProps<
  HomeProps
> = async () => ({
  props: {
    backendUrl: process.env.BACKEND_URL || 'localhost:4000',
  },
});

export default function Home({
  backendUrl,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {
  return (
      <WebSocketProvider backendUrl={backendUrl}>
        <Canvas />
      </WebSocketProvider>
  );
}
