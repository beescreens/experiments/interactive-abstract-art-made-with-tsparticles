import {
    ConnectedSocket,
    MessageBody,
    OnGatewayConnection,
    OnGatewayDisconnect,
    SubscribeMessage,
    WebSocketGateway,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';

type Point = {
    x: number;
    y: number;
};

@WebSocketGateway({
    cors: {
        origin: '*',
    },
})
export class AppGateway
    implements OnGatewayConnection, OnGatewayDisconnect {
    handleConnection(@ConnectedSocket() socket: Socket): void {
        console.log('A player has connected');
    }

    handleDisconnect(@ConnectedSocket() socket: Socket): void {
        console.log('A player has disconnected');
    }

    @SubscribeMessage('POINT')
    start(@ConnectedSocket() socket: Socket, @MessageBody() point: Point): void {
        console.log(point)
        socket.broadcast.emit('POINT', point);
    }
}