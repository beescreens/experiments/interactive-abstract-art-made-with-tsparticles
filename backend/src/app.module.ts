import { Module } from '@nestjs/common';
import { AppGateway } from './app.gateway';
import { ConfigModule } from './config/config.module';

@Module({
	imports: [ConfigModule],
	providers: [AppGateway],
})
export class AppModule {}
