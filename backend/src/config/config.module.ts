import { Module } from '@nestjs/common';
import { ConfigModule as ConfigurationModule } from '@nestjs/config';
import * as Joi from 'joi';

@Module({
  imports: [
    ConfigurationModule.forRoot({
      validationSchema: Joi.object({
        PORT: Joi.number().default(4000),
      }),
      validationOptions: {
        allowUnknown: true,
        abortEarly: false,
      },
    }),
  ],
  exports: [ConfigurationModule],
})
export class ConfigModule {}
